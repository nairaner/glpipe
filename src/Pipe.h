#ifndef PIPE_H
#define PIPE_H

#include <vector>
#include <glm/glm.hpp>

#define SEGMENTS 120
class Pipe {

public:
    Pipe(float length, float radius, float thickness);

    float thickness;
    float length;
    float radius;

    void generateVertices(std::vector<glm::mat2x3> &vertices, std::vector<glm::uvec3> &indices);
};


#endif
