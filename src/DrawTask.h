
#ifndef DRAWTASK_H
#define DRAWTASK_H

#include "Shader.h"

class DrawTask {
public:
    DrawTask(Shader shader, unsigned int VAO, unsigned int VBO, unsigned int EBO, unsigned int indexCount);

    ~DrawTask();
    Shader s;
    unsigned int VBO;
    unsigned int VAO;
    unsigned int indexCount;
    unsigned int EBO;
};


#endif
