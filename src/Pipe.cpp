#include <vector>
#include <glm/glm.hpp>
#include "Pipe.h"

Pipe::Pipe(float length, float radius, float thickness) {
    this->length = length;
    this->radius = radius;
    this->thickness = thickness;
}

void Pipe::generateVertices(std::vector<glm::mat2x3> &vertices, std::vector<glm::uvec3> &indices) {
    float innerRadius = radius - thickness;
    for (int i = 0; i < SEGMENTS; ++i) {
        float angle = 2*i*M_PI/SEGMENTS;
        float c,s;
        c = glm::cos(angle);
        s = glm::sin(angle);
        glm::mat2x3 vec = {{ s*innerRadius, length, c*innerRadius }, { 0.0f, 0.0f, 0.0f}  };
        vertices.emplace_back(vec);
        vec[0] = { s*radius, length, c*radius };
        vertices.emplace_back(vec);
    }
    for(auto const &vertex: vertices) {
        glm::mat2x3 vert = {{vertex[0].x, 0, vertex[0].z} , { 0.0f, 0.0f, 0.0f}  };
        vertices.emplace_back(vert);
    }
    unsigned int i;
    for (i = 0; i < 2*SEGMENTS - 2; i = i+2) {
        //bottom
        indices.insert(indices.end(),{i,1+i,2+i});
        indices.insert(indices.end(),{2+i,1+i,3+i});
        //inner
        indices.insert(indices.end(),{2+i,2*SEGMENTS+i,i});
        indices.insert(indices.end(),{2+i,2*SEGMENTS+2+i,2*SEGMENTS+i});
        //top
        indices.insert(indices.end(),{2*SEGMENTS+i,2*SEGMENTS+2+i,2*SEGMENTS+1+i});
        indices.insert(indices.end(),{2*SEGMENTS+i+1,2*SEGMENTS+2+i,2*SEGMENTS+3+i});
        //outer
        indices.insert(indices.end(),{1+i,2*SEGMENTS+1+i,3+i});
        indices.insert(indices.end(),{3+i,2*SEGMENTS+1+i,2*SEGMENTS+3+i});
    }
    indices.insert(indices.end(),{i,1+i,2+i - 2*SEGMENTS});
    indices.insert(indices.end(),{2+i - 2*SEGMENTS,1+i,3+i - 2*SEGMENTS});
    //inner
    indices.insert(indices.end(),{2+i - 2*SEGMENTS,2*SEGMENTS+i,i});
    indices.insert(indices.end(),{2+i - 2*SEGMENTS,2+i,2*SEGMENTS+i});
    //top
    indices.insert(indices.end(),{2*SEGMENTS+i,2+i,2*SEGMENTS+1+i});
    indices.insert(indices.end(),{2*SEGMENTS+i+1,2+i,3+i});
    //outer
    indices.insert(indices.end(),{1+i,2*SEGMENTS+1+i,3+i - 2*SEGMENTS});
    indices.insert(indices.end(),{3+i - 2*SEGMENTS,2*SEGMENTS+1+i,3+i});

    // Calculate vertex normals
    for (const auto &triangle: indices) {
        glm::vec3 p1,p2,p3;
        p1 = vertices[triangle.x][0];
        p2 = vertices[triangle.y][0];
        p3 = vertices[triangle.z][0];
        glm::vec3 edge1 = {p1.x-p2.x, p1.y-p2.y, p1.z-p2.z};
        glm::vec3 edge2 = {p3.x-p2.x, p3.y-p2.y, p3.z-p2.z};
        glm::vec3 tNormal = glm::normalize(glm::cross(edge1,edge2));
        vertices[triangle.x][1] += tNormal;
        vertices[triangle.y][1] += tNormal;
        vertices[triangle.z][1] += tNormal;
    }
}
