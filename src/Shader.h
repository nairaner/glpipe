#ifndef SHADER_H
#define SHADER_H

#include <glm/mat4x4.hpp>
#include <GL/glew.h>
#include <string>
#include <iostream>
class Shader {

public:
    unsigned int ID;
    Shader(const GLchar* vertexPath, const GLchar* fragmentPath);

    Shader();

    void use() const;
    void setBool(const std::string &name, bool value) const;
    void setInt(const std::string &name, int value) const;
    void setFloat(const std::string &name, float value) const;
    void setMat4(const std::string &name, glm::mat4 value) const;

    void checkCompileErrors(unsigned int shader, std::string type);

};


#endif
