#include "DrawTask.h"

DrawTask::~DrawTask() {
    glDeleteVertexArrays(1,&VAO);
    glDeleteBuffers(1, &VBO);
}

DrawTask::DrawTask(Shader shader, unsigned int VAO, unsigned int VBO, unsigned int EBO, unsigned int indexCount) {
    this->s = shader;
    this->VAO = VAO;
    this->EBO = EBO;
    this->VBO = VBO;
    this->indexCount = indexCount;
}
