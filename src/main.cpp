#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "Shader.h"
#include "DrawTask.h"
#include "Pipe.h"
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>

GLFWwindow* window;

void initWindow();

void createRenderedObject();

void createExample();

void updateView();

void onScroll(GLFWwindow *pWwindow, double xoffset, double yoffset);

unsigned int VAO;
unsigned int VBO;
std::vector<DrawTask> drawTasks;
glm::mat4 projection, model, view;
// Pipe default parameters
float length = 100, radius = 20, thickness = 5;

float mouseX, mouseY;

glm::vec3 eyeRotation = { 1.0f, 0.0f, 0.0f };
glm::vec3 lookAtPoint = { 0.0f, 0.0f, 0.0f };

int main(int argc,char* argv[]) {
    if (argc == 1)
        std::cout << "No argumets passed, using default values";
    else if (argc == 4) {
        try {
            thickness = std::stof(argv[1]);
            float internalDiameter = std::stof(argv[2]);
            radius = internalDiameter/2 + thickness;
            length = std::stof(argv[3]);
        }
        catch (std::invalid_argument &e) {
            std::cout << "Pass correct arguments: " << e.what();
            exit(-1);
        }

    } else {
        std::cout << "Pass either 3 arguments or none";
    }

    initWindow();
    createRenderedObject();
    //createExample();

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    glEnable(GL_CULL_FACE);
    projection = glm::perspective(glm::radians(60.0f), (float) 1280 / (float)720, 0.1f*length, 100.0f*length);
    model = glm::mat4(1.0f);
    glClearColor(0.4f, 0.4f, 0.6f, 1.0f);
    while (!glfwWindowShouldClose(window))
    {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        for (auto const& dt: drawTasks) {
            dt.s.use();
            updateView();
            GLuint a = glGetError();
            if (dt.EBO) {
                glBindVertexArray(dt.VAO);
                glBindBuffer(GL_ARRAY_BUFFER, dt.VBO);
                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, dt.EBO);
                glDrawElements(GL_TRIANGLES, dt.indexCount, GL_UNSIGNED_INT, (void*)0);
            }
            else {
                glBindVertexArray(dt.VAO);
                glDrawArrays(GL_TRIANGLES, 0, dt.indexCount);
                glBindVertexArray(0);
            }
        }

        glfwSwapBuffers(window);
        glfwPollEvents();
    }
    drawTasks.clear();
    glfwTerminate();

    return 0;
}

void updateView() {
    double newMouseX, newMouseY;
    glfwGetCursorPos(window, &newMouseX, &newMouseY);

    float xDelta, yDelta;
    xDelta = (float)newMouseX - mouseX;
    yDelta = (float)newMouseY - mouseY;
    const float R_SPEED = 0.3f;

    mouseX = (float)newMouseX;
    mouseY = (float)newMouseY;
    lookAtPoint = {0, length/2, 0};
    if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT))
    {
        eyeRotation[0] = glm::clamp(eyeRotation[0] + R_SPEED*glm::radians(yDelta), 0.01f, 3.14f);
        eyeRotation[2] = eyeRotation[2] + R_SPEED*glm::radians(xDelta);
    }
    glm::vec3 eyePosition = {glm::cos(eyeRotation[2])*glm::sin(eyeRotation[0]), glm::sin(eyeRotation[2])*glm::sin(eyeRotation[0]), glm::cos(eyeRotation[0])};
    eyePosition *= length + radius*2;
    view = glm::lookAt(eyePosition + lookAtPoint, lookAtPoint, glm::vec3(0.0f, 0.0f, 1.0f));
    model = glm::mat4(1.0f);

    // Set uniforms
    drawTasks[0].s.setMat4("modelview", model*view);
    drawTasks[0].s.setMat4("projection", projection);
    drawTasks[0].s.setMat4("normalMat", glm::inverseTranspose(model*view));
}

void createRenderedObject() {


    Pipe pipe(length, radius,thickness);
    std::vector<glm::mat2x3> vertices;
    std::vector<glm::uvec3> indices;
    pipe.generateVertices(vertices, indices);

    GLuint EBO;
    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);


    glGenBuffers(1, &VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::mat2x3), vertices.data(), GL_STATIC_DRAW);

    glGenBuffers(1, &EBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(glm::uvec3), indices.data(), GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3*sizeof(float)));
    glEnableVertexAttribArray(1);

    drawTasks.emplace_back(Shader("shaders/shader.vert", "shaders/shader.frag"), VAO, VBO, EBO, indices.size() * 3);
    updateView();
}

void createExample() {
    float vertices[] = {
            0.0f, 0.5f, 0.0f,
            -0.5f, -0.5f, 0.0f,
            0.5f, -0.5f, 0.0f
    };
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(0,3,GL_FLOAT, GL_FALSE, 3*sizeof(float), nullptr);
    glEnableVertexAttribArray(0);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    drawTasks.emplace_back(Shader("shaders/shader.vert", "shaders/shader.frag"), VAO, VBO,0, 0);

}

void initWindow() {
    if (!glfwInit())
    {
        fprintf( stderr, "Failed to initialize GLFW\n" );
        exit(-1);
    }
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR,4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR,2);
    glfwWindowHint(GLFW_OPENGL_PROFILE,GLFW_OPENGL_CORE_PROFILE);

    window = glfwCreateWindow( 1280, 720, "GLPipe", nullptr, nullptr);
    if (!window)
    {
        std::cerr << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        exit(-1);
    }
    glfwSetScrollCallback(window, onScroll);
    glfwMakeContextCurrent(window);
    glewInit();
}

void onScroll(GLFWwindow* window, double xoffset, double yoffset) {
    radius = glm::max(radius + yoffset, (double)thickness+0.01);
    drawTasks.pop_back();
    createRenderedObject();
    updateView();
    return;
}

