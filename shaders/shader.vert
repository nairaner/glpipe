#version 420 core
layout(location = 0) in vec3 inputPos;
layout(location = 1) in vec3 inputNormal;

layout(location = 0) out vec3 vertPos;
layout(location = 1) out vec3 normalInterp;

uniform mat4 projection, modelview, normalMat;
void main() {
	gl_Position = projection * modelview * vec4(inputPos, 1.0);
    vec4 vertPos4 = modelview * vec4(inputPos, 1.0);
    vertPos = vec3(vertPos4) / vertPos4.w;
    normalInterp = vec3(normalMat * vec4(inputNormal, 0.0));
}
